module.exports = function myReduce(elements, cb, startingValue) {
  let accumulator = startingValue;
  for (let index = 0; index < elements.length; index++) {
    accumulator = accumulator
      ? cb(accumulator, elements[index], index, elements)
      : elements[index];
  }

  return accumulator;
};
