module.exports = function myFilter(elements, cb) {
  let result = [];
  for (let index = 0; index < elements.length; index++) {
    if (cb(elements[index], index, elements)) {
      result.push(elements[index]);
    }
  }

  return result;
};
