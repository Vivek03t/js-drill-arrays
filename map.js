module.exports = function myMap(elements, callback) {
  let temp = [];
  for (let index = 0; index < elements.length; index++) {
    temp.push(callback(elements[index], index, elements));
  }
  return temp;
};
