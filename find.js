module.exports = function myFind(elements, cb) {
  if (!cb || typeof cb !== "function") throw TypeError();

  for (let index = 0; index < elements.length; index++) {
    if (cb(elements[index], index, elements)) {
      return elements[index];
    }
  }
};
