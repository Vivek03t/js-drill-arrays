module.exports = function myFlatten(elements) {
  let result = [];
  for (let index = 0; index < elements.length; index++) {
    if (Array.isArray(elements[index])) {
      result = result.concat(myFlatten(elements[index]));
    } else {
      result.push(elements[index]);
    }
  }
  return result;
};
