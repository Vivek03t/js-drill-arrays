const myForEach = require("../each");

const items = [1, 2, 3, 4, 5, 5];

myForEach(items, (number, index, arr) => console.log(number));
