const myMap = require("../map");

const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code.

const squareNumber = myMap(items, (num, index, arr) => {
  return num * num;
});
console.log(squareNumber);
