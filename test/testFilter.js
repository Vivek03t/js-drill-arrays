const myFilter = require("../filter");

const items = [1, 2, 3, 4, 5, 5];

const filterLessThan5 = myFilter(items, (num) => num < 5);

console.log(filterLessThan5);
