const myFlatten = require("../flatten");

const nestedArray = [1, [2], [[3]], [[[4]]]];

const flattenedArray = myFlatten(nestedArray);
console.log(flattenedArray);
