const myReduce = require("../reduce");

const items = [1, 2, 3, 4, 5, 5];

const arraySum = myReduce(items, (acc, curr) => acc + curr, 0);
console.log(arraySum);
